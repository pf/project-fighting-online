# À faire

- [ ] Lorsqu'on meurt, le jeu s'arrête avec un message de mort ;

- [ ] Les projectiles entre en collision avec les murs ;

- [ ] Les grenades rebondissent contres les murs et blocs ;

- [ ] Les fumigènes rebondissent contres les murs et blocs ;

- [ ] Synchroniser les projectiles, blocs, déplacements et autres sur le
  réseau ;

- [ ] Corriger la génération du niveau (il devrait être différent à chaque
  lancement du serveur) ;

- [ ] Possibilité de jouer depuis son téléphone portable avec des contrôles
  appropriés.
