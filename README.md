# Project Fighting

Un jeu JavaScript multijoueur.

## Compilation

```
bash env PORT=3000 npm run start
```

## Déploiement sur now (tests publics)

```
bash now --public -e PORT=80
```

## Déploiement sur Heroku

```
git push heroku
```

## Inspirations

- <https://twitter.com/LootBndt/status/926234010531098624>.

## Actions utilisateur

- `🡠`, `🡢`, `🡡`, `🡣` pour se déplacer respectivement à gauche, à droite, en
  haut et en bas ;
- `Clic gauche` pour tirer ;
- `Roulement molette` pour changer d'arme ;
- `Déplacement curseur` pour orienter le tir.

## Armes

- **Dynamite :** objet posé au sol, explose après une durée déterminée ;

- **Lance-flammes :** lance des flammes (particules), elles font peu de dégâts
  mais couvrent une large zone en continue ;

- **Grenade :** rebondie contre les murs explose après une durée déterminée ;

- **Pistolet :** tir simple à cadence lente ;

- **Mine :** objet posé au sol, explose peu de temps après qu'un joueur passe
  dessus ;

- **Bazooka :** tir avec explosion au contact d'un joueur, d'un mur ou tout
  objet sur la carte ;

- **Fumigène :** rebondie contre les murs, explose après une durée déterminée
  et déploie une épaisse fumée qui empêche de voir une partie du terrain
  (particules qui ne causent pas de dégâts) ;

- **Fusil à pompe :** tirs multiples à cadence lente ;

- **Mitraillette :** tir simple avec cadence rapide.

La puissance de tir des grenades et des fumigènes dépend de la distance entre
le joueur qui effectue le lancer et le pointeur (curseur).

Deux questions sont encore en suspend :

- Faut-il ajouter une arme « boomerang » ? Cela pourrait être fun, mais il faut
  définir son comportement : le boomerang passe-t-il à travers les murs et
  objets ? combien cause-t-il de dégâts ? comment définir sa trajectoire ?
  sachant que le boomerang aura une trajectoire qui la ramènera à son
  propriétaire, comment être sûr que son affichage soit bien synchronisé chez
  tous les joueurs sachant que sa position d'affichage dépend en partie de la
  position du propriétaire ? etc. 

- Faut-il transformer le fumigène (fumée qui ne cause pas de dégâts) en
  lacrymogène (fumée qui cause des dégâts) ?

## Explosion

Le terme « explosion » définit un ensemble de particules projetées dans toutes
les directions (à 360°).

## Particules

Les particules font des dégâts chez les joueurs, il faut donc que la position
des particules soit identique chez tous les joueurs et sur le serveur. Cela,
peu importe l'origine de la particule : d'une dynamite, d'une grenade, d'une
mine, de l'explosition du lancer de bazooka, du lance-flammes ou de l'explosion
d'un baril. Les deux exceptions sont : la trainée du lancer de bazooka et la
fumée du fumigène, qui ne feront pas de dégâts.

Afin que les positions, déplacements et couleurs des particules soient
identiques chez les clients et sur le serveur, une graine devra être générée
lors du tir et sera utilisée par le client et par le serveur pour générer des
valeurs aléatoires identiques entre le client et le serveur.

## Blocs

- **Barils :** explose quand n'a plus de vie ;

- **Caisses :** sont comme des murs. Si possible : faire en sorte que les
  joueurs puissent pousser les caisses.

## Communication client-serveur

### Envoi de données depuis le client vers le serveur

Le client envoie les évènements suivants :

- Son entrée en jeu ;
- Ses déplacements ;
- Son tir ;
- Son angle (regard) ;
- Sa déconnexion.

On pourrait en imaginer d'autres, comme le changement d'arme par exemple, mais
ce n'est pas prévu pour l'instant. Par la suite, dans l'entrée en jeu, viendra
le choix du pseudonyme et le choix de l'apparence du personnage.

### Envoi de donnés depuis le serveur vers le client

Le serveur est lui un peu plus bavard :

- Données initiales (terrain, blocs, projectiles, joueurs...) ;
- Connexion d'un nouveau joueur ;
- Déconnexion d'un joueur ;
- Déplacement d'un joueur ;
- Changement d'angle d'un joueur ;
- Tir d'un joueur ;
- Synchronisation (à détailler, mais le but est de s'assurer que le joueur est
  toujours bien synchronisé avec le serveur) ;
- Ping/pong ;
- (à compléter ?).

### À noter

Tout cela doit être extrêmement bien synchronisé. Par exemple, le tir étant
rapide, la balle a le temps de se déplacer entre l'envoi de la donnée du client
vers le serveur, et de nouveau entre le serveur vers les clients. Il est donc
nécessaire de bien enregistrer le temps des actions.

Aussi, les particules doivent être identiques (même position, même taille, même
couleur, etc.) chez tous les clients. Pour cela, une graine devra être
transmise avec tout ce qui en génère (explosition de mine, explosion de
dynamite, lance-flammes, etc.).

Un tir ne fait pas forcément référence à un tir de balle. Le dépôt d'une mine
est un tir au même titre que les autres, par exemple. Le tir fait référence à
l'action qu'exerce le joueur en actionnant l'arme qu'il a en main (via un clic
gauche).
