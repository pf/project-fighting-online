const path = require('path')

module.exports = {
  entry: './src/client/main.ts',

  module: {
    rules: [
      {
        test: /\.ts$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  },

  resolve: {
    extensions: [ '.ts', ],
  },

  output: {
    filename: 'app.js',
    path: path.resolve(__dirname, 'public'),
  },
}
