import Rect from '../src/common/Rect';
import Hit from '../src/common/Hit';

test('rect hit rect', () => {
  const r1 = new Rect(0, 0, 10, 10)
  const r2 = new Rect(5, 5, 10, 10)

  const collision = Hit.rectHitRect(r1, r2)

  expect(collision).toBe(true)
})
