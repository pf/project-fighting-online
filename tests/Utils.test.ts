import { clamp } from '../src/common/Util'

test('clamp', () => {
  expect(clamp(5, 1, 9)).toBe(5)
  expect(clamp(0, 1, 9)).toBe(1)
  expect(clamp(10, 1, 9)).toBe(9)

  expect(clamp(5, 9, 1)).toBe(1)
})

test('rand', () => {
  // ¯\_(ツ)_/¯
})
