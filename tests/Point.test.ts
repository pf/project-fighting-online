import Point from './../src/common/Point'

let point: Point = new Point(0, 0)

beforeEach(() => {
  point = new Point(0, 0)
})

test('constructor', () => {
  expect(point.getX()).toBe(0)
  expect(point.getY()).toBe(0)
})

test('setX/getX', () => {
  point.setX(10)

  expect(point.getX()).toBe(10)
})

test('setY/getY', () => {
  point.setY(20)

  expect(point.getY()).toBe(20)
})

test('clone', () => {
  const point2 = point.clone()

  expect(point2.getX()).toBe(point.getY())
  expect(point2.getY()).toBe(point.getY())
})
