import { v4 as uuidv4 } from 'uuid'
import { PlayerEvent } from '../common/Event'
import BaseGame from '../common/Game'
import { FirePacket, firePacketToWeapon, PlayerAnglePacket, PlayerPositionPacket } from '../common/Packet'
import Player from '../common/Player'
import WebSocket from './WebSocketServer'

export default class Game extends BaseGame {
  private ws: any

  public constructor(server: any) {
    super()

    this.initSocket(server)
  }

  public initSocket(server: any): void {
    this.ws = new WebSocket(server)

    // Connexion d'un client
    this.ws.on('connection', (client: any) => {
      // Déconnexion de l'utilisateur
      client.on('close', () => {
        const player = this.getPlayer(client.uuid)

        this.removePlayer(player)

        client.broadcast(PlayerEvent.Quit, {
          uuid: client.uuid,
        })
      })

      client.on(PlayerEvent.Joined, (data: any) => {
        const players: { [key: string]: object } = {}
        const commonPlayers = this.getPlayers()

        for (const uuid in commonPlayers) {
          if (!commonPlayers .hasOwnProperty(uuid))
            continue

          const player = commonPlayers[uuid]

          players[uuid] = {
            x: player.getPosition().getX(),
            y: player.getPosition().getY(),
            color: player.getColor(),
          }
        }

        const blocks: Array<object> = []
        const commonBlocks = this.getBlocks()

        for (const block of commonBlocks) {
          blocks.push({
            x: block.getPosition().getX(),
            y: block.getPosition().getY(),
          })
        }

        client.uuid = uuidv4()

        const clientPlayer = new Player(this)
        clientPlayer.setUuid(client.uuid)

        client.emit(PlayerEvent.Joined, {
          x: clientPlayer.getPosition().getX(),
          y: clientPlayer.getPosition().getY(),
          color: clientPlayer.getColor(),
          uuid: client.uuid,
          yours: true,
          mapSeed: this.getMap().getSeed(),
          players: players,
          blocks: blocks,
        })

        client.broadcast(PlayerEvent.Joined, {
          x: clientPlayer.getPosition().getX(),
          y: clientPlayer.getPosition().getY(),
          color: clientPlayer.getColor(),
          uuid: client.uuid,
          yours: false,
        })

        this.addPlayer(clientPlayer)
      })

      client.on(PlayerEvent.ChangePosition, (data: PlayerPositionPacket) => {
        const player = this.getPlayer(client.uuid)

        player.getTargetPosition().setX(data.x)
        player.getTargetPosition().setY(data.y)

        client.broadcast(PlayerEvent.ChangePosition, {
          uuid: client.uuid,
          x: player.getTargetPosition().getX(),
          y: player.getTargetPosition().getY(),
        } as PlayerPositionPacket)
      })

      client.on(PlayerEvent.Fire, (data: FirePacket) => {
        const clientTs = data.ts
        const serverTs = Date.now()
        const diffTs = serverTs - clientTs

        /*

        // Tirer une balle

        const bullet = new Bullet(data.x, data.y, data.angle, data.speed)
        bullet.setPosition(data.x, data.y)

        bullet.update(diffTs / 1000)

        client.broadcast(PlayerEvent.Fire, {
          x: bullet.getPosition().getX(),
          y: bullet.getPosition().getY(),
          angle: bullet.getAngle(),
          speed: bullet.getSpeed(),
          ts: serverTs
        })

        this.addProjectile(bullet)

        */

        firePacketToWeapon(data, this)

        /*

        const mine = new Mine(this, data.x, data.y)
        weapon.setPosition(new Point(data.x, data.y))

        mine.update(diffTs / 1000)

        */

        client.broadcast(PlayerEvent.Fire, data)
      })

      client.on(PlayerEvent.ChangeAngle, (data: PlayerAnglePacket) => {
        client.broadcast(PlayerEvent.ChangeAngle, {
          uuid: client.uuid,
          angle: data.angle,
        })
      })
    })
  }

  public update(): void {
    const update = () => {
      super.update()

      setTimeout(update, 1000 / 10)
    }

    update()
  }
}
