import * as WebSocket from 'uws'
import { Server } from 'uws'

class WebSocketClient {
  private server: any
  private ws: any
  private callbacks: any

  public constructor(server: any, ws: any) {
    this.server = server
    this.ws = ws
    this.callbacks = {}

    this.ws.on('message', (d: any) => {
      const data = JSON.parse(d)

      if (typeof this.callbacks[data.type] === 'undefined')
        return

      this.callbacks[data.type].forEach((callback: any) => {
        callback(data.data)
      })
    })

    this.ws.on('close', () => {
      if (typeof this.callbacks['close'] === 'undefined')
        return

      this.callbacks['close'].forEach((callback: any) => {
        callback()
      })
    })
  }

  public emit(type: string, data: any): void {
    if (this.ws.readyState !== WebSocket.OPEN)
      return

    this.ws.send(JSON.stringify({
      type: type,
      data: data,
    }))
  }

  public broadcast(type: string, data: any): void {
    this.server.emit(type, data, this)
  }

  public on(type: string, callback: any): void {
    if (typeof this.callbacks[type] === 'undefined')
      this.callbacks[type] = []

    this.callbacks[type].push(callback)
  }
}

export default class WebSocketServer {
  private ws: any
  private callbacks: any
  private clients: any

  public constructor(server: any) {
    this.ws = new Server({ server })
    this.callbacks = {}
    this.clients = []

    this.ws.on('connection', (ws: any) => {
      const client = new WebSocketClient(this, ws)

      this.clients.push(client)

      if (typeof this.callbacks['connection'] === 'undefined')
        return

      this.callbacks['connection'].forEach((callback: any) => {
        callback(client)
      })
    })

    this.ws.on('message', (d: any) => {
      const data = JSON.parse(d)

      if (typeof this.callbacks[data.type] === 'undefined')
        return

      this.callbacks[data.type].forEach((callback: any) => {
        callback(data.data)
      })
    })
  }

  public emit(type: string, data: any, excluded: any): void {
    this.clients.forEach((client: any) => {
      if (client === excluded)
        return

      client.emit(type, data)
    })
  }

  public on(type: string, callback: any): void {
    if (typeof this.callbacks[type] === 'undefined')
      this.callbacks[type] = []

    this.callbacks[type].push(callback)
  }
}
