import * as express from 'express'
import * as path from 'path'
import Game from './Game'

// Création du serveur
const app = express()

// Emplacement des fichiers statiques
app.use(express.static('public'))

// Page racine
app.get('/', (request, response) => {
  response.sendFile(path.resolve(`${__dirname}/../../../views/index.html`))
})

// Lancement du serveur
const server = app.listen(process.env.PORT, () => {
  console.log(`L'application a démarrée sur le port ${process.env.PORT}`)

  const serverGame = new Game(server)
  serverGame.start()
})
