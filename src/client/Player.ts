import { PlayerEvent } from '../common/Event'
import { PlayerPositionPacket } from '../common/Packet'
import BasePlayer from '../common/Player'
import Game from './Game'

export default class Player extends BasePlayer {
  private socket: any

  public constructor(game: Game, socket: any) {
    super(game)

    this.socket = socket
  }

  public update(dt: number): void {
    super.update(dt)

    if (this.socket) {
      if (
        this.getTargetPosition().getX() !== this.getPosition().getX()
        || this.getTargetPosition().getY() !== this.getPosition().getY()
      ) {
        this.socket.emit(PlayerEvent.ChangePosition, {
          x: this.getTargetPosition().getX(),
          y: this.getTargetPosition().getY(),
        } as PlayerPositionPacket)
      }
    }
  }

  protected updateDir(dt: number): void {
    super.updateDir(dt)

    if (this.getDir() !== this.getPreviousDir()) {
      if (this.socket) {
        this.socket.emit(PlayerEvent.ChangeAngle, {
          angle: this.getAngle(),
        })
      }

      this.setPreviousDir(this.getDir())
    }
  }
}
