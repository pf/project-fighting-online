export default class WebSocketWrapper {
  private socket: any
  private callbacks: any

  public constructor(url: string) {
    const socket = new WebSocket(url)

    this.socket = socket
    this.callbacks = {}

    this.socket.addEventListener('open', () => {
      if (typeof this.callbacks['open'] !== 'undefined')
        this.callbacks['open']()
    })

    this.socket.addEventListener('close', () => {
      if (typeof this.callbacks['close'] !== 'undefined')
        this.callbacks['close']()
    })

    this.socket.addEventListener('message', (d: any) => {
      const data = JSON.parse(d.data)

      if (typeof this.callbacks[data.type] !== 'undefined')
        this.callbacks[data.type](data.data)
      else
        throw new Error(`Socket callback ${data.type} unknown`)
    })
  }

  public on(type: string, callback: any): void {
    this.callbacks[type] = callback
  }

  public emit(type: string, data: any = {}): void {
    this.socket.send(JSON.stringify({
      type: type,
      data: data,
    }))
  }
}
