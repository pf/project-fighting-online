import Mine from '../common/block/Mine'
import Camera from '../common/Camera'
import { PlayerEvent } from '../common/Event'
import BaseGame from '../common/Game'
import Hit from '../common/Hit'
import { firePacketToWeapon, PlayerAnglePacket, PlayerPositionPacket } from '../common/Packet'
import Point from '../common/Point'
import Rect from '../common/Rect'
import Inventory from '../common/ui/Inventory'
import PlayerInfo from '../common/ui/PlayerInfo'
import { Key, Keyboard } from './Keyboard'
import Mouse from './Mouse'
import Player from './Player'
import WebSocketWrapper from './WebSocketWrapper'
import { WheelDirection } from './WheelDirection'

export default class Game extends BaseGame {
  private can: HTMLCanvasElement
  private ctx: CanvasRenderingContext2D
  private localPlayer: Player | null
  // private socket: WebSocketWrapper | null
  private playerInfo: PlayerInfo | null
  private inventory: Inventory | null
  private camera: Camera
  private mouse: Mouse
  private keyboard: Keyboard

  public constructor() {
    super()

    this.can = document.getElementById('game') as HTMLCanvasElement
    this.ctx = this.can.getContext('2d') as CanvasRenderingContext2D

    // this.ctx.imageSmoothingEnabled = false

    this.can.width = BaseGame.WIDTH
    this.can.height = BaseGame.HEIGHT

    this.localPlayer = null
    // this.lastUpdate = 0

    this.mouse = new Mouse(this)
    this.keyboard = new Keyboard()

    this.playerInfo = null

    this.setSocket(null)

    this.inventory = null

    this.camera = new Camera(0, 0, 640, 480)

    this.getImageManager().setPath('assets/img')
    this.getImageManager().setCallback(() => {
      this.runSocket()
      this.getMap().generateRender(this)
    })
    this.getImageManager().load({
      'player': 'player.png',
      'block/dynamite': 'block/dynamite.png',
      'block/mine': 'block/mine.png',
      'block/barrel': 'block/barrel.png',
      'block/crate': 'block/crate.png',
      'weapons-miniatures': 'weapons-miniatures.png',
      'projectile/bullet': 'projectile/bullet.png',
      'projectile/rocket': 'projectile/rocket.png',
      'projectile/grenade': 'projectile/grenade.png',
      'projectile/smoke-grenade': 'projectile/smoke-grenade.png',
      'ground': 'ground.png',
      'wall': 'wall.png',
    })
  }

  public runSocket(): void {
    let socket: WebSocketWrapper | null

    const url = new URL(location.href)

    if (url.protocol === 'https:')
      socket = new WebSocketWrapper(`wss://${url.host}/`)
    else
      socket = new WebSocketWrapper(`ws://${url.host}/`)

    this.setSocket(socket)

    if (socket == null)
      return

    socket.on('open', () => {
      if (socket == null)
        return

      socket.emit(PlayerEvent.Joined)
    })

    socket.on(PlayerEvent.Joined, (data: any) => {
      const player = new Player(this, data.yours ? socket : null)
      player.setUuid(data.uuid)
      player.setPosition(new Point(data.x, data.y))
      player.setColor(data.color)

      if (data.yours) {
        this.localPlayer = player

        this.inventory = new Inventory(this.localPlayer)

        this.playerInfo = new PlayerInfo(this.localPlayer)

        // Ajoute les joueurs connectés précemment à la partie
        for (const uuid in data.players) {
          if (!data.players.hasOwnProperty(uuid))
            continue

          const p = data.players[uuid]

          const position = new Point(p.x, p.y)

          const pp = new Player(this, null)
          pp.setUuid(uuid)
          pp.setPosition(position)
          pp.setTargetPosition(position)
          pp.setColor(p.color)

          this.addPlayer(pp)
        }

        for (const b of data.blocks) {
          // if (!data.blocks.hasOwnProperty(i))
          //  continue

          const position = new Point(b.x, b.y)

          const bb = new Mine(this, position.getX(), position.getY())

          this.addBlock(bb)
        }

        this.getMap().setSeed(data.seed)
        this.getMap().generate()
        this.getMap().generateRender(this)
      }

      this.addPlayer(player)
    })

    socket.on(PlayerEvent.Quit, (data: any) => {
      const player = this.getPlayer(data.uuid)

      this.removePlayer(player)
    })

    socket.on(PlayerEvent.ChangePosition, (data: PlayerPositionPacket) => {
      const player = this.getPlayer(data.uuid)

      player.getTargetPosition().setX(data.x)
      player.getTargetPosition().setY(data.y)
    })

    socket.on(PlayerEvent.Fire, (data: any) => {
      const serverTs = data.ts
      const clientTs = Date.now()
      const diffTs = clientTs - serverTs

      firePacketToWeapon(data, this)
    })

    socket.on(PlayerEvent.ChangeAngle, (data: PlayerAnglePacket) => {
      const player = this.getPlayer(data.uuid)

      player.setAngle(data.angle)
    })
  }

  public update(): void {
    super.update()

    const socket = this.getSocket()

    if (this.localPlayer) {
      this.camera.setPosition(new Point(
        -(this.localPlayer.getX() + (this.localPlayer.getWidth() / 2)) + (640 / 2),
        -(this.localPlayer.getY() + (this.localPlayer.getHeight() / 2)) + (480 / 2),
      ))

      const angle = Point.angleBetween(
        this.localPlayer.getPosition(),
        this.getMousePosition(),
      ) / Math.PI

      this.localPlayer.setAngle(angle)
    }

    if (this.localPlayer) {
      const move = new Point(0, 0)

      if (this.keyboard.down(Key.Right) || this.keyboard.down(Key.D))
        move.addX(1)

      if (this.keyboard.down(Key.Left) || this.keyboard.down(Key.Q) || this.keyboard.down(Key.A))
        move.addX(-1)

      if (this.keyboard.down(Key.Up) || this.keyboard.down(Key.Z) || this.keyboard.down(Key.W))
        move.addY(-1)

      if (this.keyboard.down(Key.Down) || this.keyboard.down(Key.S))
        move.addY(1)

      this.localPlayer.move(move)
    }

    if (this.localPlayer)
      this.collide(this.getDt(), this.localPlayer)

    if (socket && this.localPlayer) {
      if (this.mouse.down()) {
        const mousePosition = this.mouse.getCanvasCoordinates(this.camera)

        this.localPlayer.fire(mousePosition)
      }
    }

    if (this.localPlayer) {
      const direction = this.mouse.wheel()

      if (direction === WheelDirection.Bottom || direction === WheelDirection.Top) {
        const weaponIndex = this.localPlayer.getCurrentWeaponIndex()
        const nbWeapons = this.localPlayer.getWeapons().length

        let currentWeaponIndex = weaponIndex

        currentWeaponIndex += WheelDirection.toInt(direction)

        if (currentWeaponIndex > nbWeapons - 1)
          currentWeaponIndex = nbWeapons - 1
        else if (currentWeaponIndex < 0)
          currentWeaponIndex = 0

        this.localPlayer.setCurrentWeaponIndex(currentWeaponIndex)
      }

      const players = this.getPlayers()

      for (const i in players) {
        if (!players.hasOwnProperty(i))
          continue

        const player = players[i]
        player.render(this.ctx)
      }
    }

    this.mouse.update()

    requestAnimationFrame(() => this.update())
    requestAnimationFrame(() => this.render())
  }

  public collide(dt: number, p: Player): void {
    const blocks = this.getMap().getBlocksAround(p.getCircle())

    const vx = p.getVelocity().getX() * dt
    const vy = p.getVelocity().getY() * dt

    // let vx = p.xnorm * Player.maxspeed * dt
    // let vy = p.ynorm * Player.maxspeed * dt

    const c = new Rect(p.getX() + vx, p.getY() + vy, p.getWidth(), p.getHeight())
    const cx = new Rect(p.getX() + vx, p.getY(), p.getWidth(), p.getHeight())
    const cy = new Rect(p.getX(), p.getY() + vy, p.getWidth(), p.getHeight())

    for (const b of blocks) {
      if (Hit.rectHitRect(c, b)) {
        if (Hit.rectHitRect(cx, b)) {
          if (p.getX() > p.getTargetPosition().getX()) {
            p.getTargetPosition().setX(b.getX() + b.getWidth())
            p.setX(b.getX() + b.getWidth())
            p.getVelocity().setX(0)
          } else if (p.getX() < p.getTargetPosition().getX()) {
            p.getTargetPosition().setX(b.getX() - b.getWidth())
            p.setX(b.getX() - b.getWidth())
            p.getVelocity().setX(0)
          }
        }

        if (Hit.rectHitRect(cy, b)) {
          if (p.getY() > p.getTargetPosition().getY()) {
            p.getTargetPosition().setY(b.getY() + b.getHeight())
            p.setY(b.getY() + b.getHeight())
            p.getVelocity().setY(0)
          } else if (p.getY() < p.getTargetPosition().getY()) {
            p.getTargetPosition().setY(b.getY() - b.getHeight())
            p.setY(b.getY() - b.getHeight())
            p.getVelocity().setY(0)
          }
        }
      }
    }
  }

  public render(): void {
    this.ctx.clearRect(0, 0, BaseGame.WIDTH, BaseGame.HEIGHT)

    this.ctx.fillStyle = 'rgb(18, 5, 32)'
    this.ctx.fillRect(0, 0, BaseGame.WIDTH, BaseGame.HEIGHT)

    this.ctx.save()
    this.ctx.translate(this.camera.getX(), this.camera.getY())

    this.renderMap()

    for (const block of this.getBlocks())
      block.render(this.ctx)

    const players = this.getPlayers()

    for (const i in players) {
      if (!players.hasOwnProperty(i))
        continue

      const player = players[i]
      player.render(this.ctx)
    }

    for (const projectile of this.getProjectiles())
      projectile.render(this.ctx)

    for (const particle of this.getParticles())
      particle.render(this.ctx)

    this.ctx.restore()

    if (this.playerInfo)
      this.playerInfo.render(this.ctx)

    if (this.inventory != null)
      this.inventory.render(this.ctx)
  }

  public renderMap(): void {
    const render = this.getMap().getRender()

    if (render)
      this.ctx.drawImage(render.canvas, 0, 0)
  }

  public getLocalPlayer(): Player | null {
    return this.localPlayer
  }

  public getMousePosition(): Point {
    return this.mouse.getCanvasCoordinates(this.camera)
  }

  public getCanvas(): HTMLCanvasElement {
    return this.can
  }
}
