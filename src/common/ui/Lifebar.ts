import Player from '..//Player'
import Rect from '../../common/Rect'

export default class Lifebar extends Rect {
  // Joueur lié à la barre de vue
  private player: Player

  // Lie la barre de vie au joueur, ainsi lorsque la vie du joueur change, la
  // barre de vie est automatiquement mise à jour
  // Créer une barre de vie
  constructor(player: Player) {
    super(19, 14, 60, 4)

    this.player = player
  }

  // Affiche la barre de vie
  public render(ctx: CanvasRenderingContext2D): void {
    const offset = 5

    const lifebarWidth = this.player.getHealthPercentage() * this.getWidth()

    // Fond vert transparent
    ctx.fillStyle = 'rgb(255, 255, 255, 0.3)'
    ctx.beginPath()
    ctx.moveTo(this.getX(), this.getY())
    ctx.lineTo(this.getX() + this.getWidth(), this.getY())
    ctx.lineTo(this.getX() + this.getWidth() + offset, this.getY() + this.getHeight())
    ctx.lineTo(this.getX() + offset, this.getY() + this.getHeight())
    ctx.fill()

    // Fond vert
    ctx.fillStyle = 'rgb(51, 182, 0)'
    ctx.beginPath()
    ctx.moveTo(this.getX(), this.getY())
    ctx.lineTo(this.getX() + lifebarWidth, this.getY())
    ctx.lineTo(this.getX() + lifebarWidth + offset, this.getY() + this.getHeight())
    ctx.lineTo(this.getX() + offset, this.getY() + this.getHeight())
    ctx.fill()

    // Texte
    ctx.fillText(`${this.player.getHealth()}`, this.getX() + 20, this.getY() - 10)
    // ctx.drawPixelatedText(`${this.player.getHealth()}`, this.getX() + 20, this.getY() - 10)
  }
}
