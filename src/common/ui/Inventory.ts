import Player from '../Player'

export default class Inventory {
  private player: Player

  constructor(player: Player) {
    this.player = player
  }

  public render(ctx: CanvasRenderingContext2D): void {
    ctx.save()
    ctx.translate(4, 30)

    ctx.lineWidth = 1
    ctx.strokeStyle = 'rgb(202, 84, 42)'

    const weapons = this.player.getWeapons()

    for (const weapon of weapons) {
      weapon.renderMiniature(ctx)

      if (weapon === this.player.getCurrentWeapon())
        ctx.strokeRect(0.5, 0.5, 16, 16)

      ctx.translate(0, 20)
    }

    ctx.restore()
  }
}
