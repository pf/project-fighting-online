import Player from '../Player'
import Lifebar from './Lifebar'
import WeaponLatency from './WeaponLatency'

export default class PlayerInfo {
  private lifebar: Lifebar
  private weaponLatency: WeaponLatency

  public constructor(player: Player) {
    this.lifebar = new Lifebar(player)
    this.weaponLatency = new WeaponLatency(player)
  }

  public render(ctx: CanvasRenderingContext2D): void {
    const offset = 23

    // Barre de vie et latence de tir
    ctx.save()
    ctx.translate(5, 5)

    // Fond noir transparent
    ctx.fillStyle = 'rgb(0, 0, 0, 0.8)'
    ctx.beginPath()
    ctx.moveTo(0, 0)
    ctx.lineTo(66, 0)
    ctx.lineTo(66 + offset, 20)
    ctx.lineTo(offset, 20)
    ctx.fill()

    this.lifebar.render(ctx)
    this.weaponLatency.render(ctx)

    ctx.restore()
  }
}