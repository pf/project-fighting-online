import Player from '../Player'
import Rect from '../../common/Rect'

export default class WeaponLatency extends Rect {
  // Joueur lié à la barre de vue
  private player: Player

  // Lie la barre de vie au joueur, ainsi lorsque la vie du joueur change, la
  // barre de vie est automatiquement mise à jour
  // Créer une barre de vie
  public constructor(player: Player) {
    super(8, 19, 62, 1)

    this.player = player
  }

  // Affiche la barre de vie
  public render(ctx: CanvasRenderingContext2D): void {
    const latency = this.player.getCurrentWeapon().getLatencyPercentage()

    // Barre de latence blanche
    ctx.fillStyle = 'rgb(255, 255, 255, 0.3)'
    ctx.fillRect(
      this.getX() + 1,
      this.getY() + 1,
      this.getWidth() - (latency * this.getWidth()),
      this.getHeight()
    )
  }
}
