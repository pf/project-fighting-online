import Player from '../client/Player'
import Barrel from './block/Barrel'
import Block from './block/Block'
import Crate from './block/Crate'
import BlockDynamite from './block/Dynamite'
import BlockMine from './block/Mine'
import Entity from './Entity'
import Particle from './Particle'
import Bullet from './weapon/projectile/Bullet'
import Grenade from './weapon/projectile/Grenade'
import Projectile from './weapon/projectile/Projectile'
import Rocket from './weapon/projectile/Rocket'
import SmokeGrenade from './weapon/projectile/SmokeGrenade'

class Collision {
  public static onBulletOverlapsBarrel(bullet: Bullet, barrel: Barrel): void {
    barrel.decreaseHealth(bullet.getDamages())

    bullet.remove()
  }

  public static onBulletOverlapsCrate(bullet: Bullet, crate: Crate): void {
    bullet.remove()
  }

  public static onBulletOverlapsPlayer(bullet: Bullet, player: Player): void {
    player.decreaseHealth(bullet.getDamages())

    bullet.remove()
  }

  public static onGrenadeOverlapsBlock(grenade: Grenade, block: Block): void {
    grenade.getVelocity().scale(-1)
  }

  public static onRocketOverlapsBlock(rocket: Rocket, block: Block): void {
    rocket.explode()
  }

  public static onRocketOverlapsPlayer(rocket: Rocket, player: Player): void {
    rocket.explode()
  }

  public static onSmokeGrenadeOverlapsBlock(smokeGrenade: SmokeGrenade, block: Block): void {
    smokeGrenade.getVelocity().scale(-1)
  }

  public static onProjectileOverlapsMine(projectile: Projectile, mine: BlockMine): void {
    mine.explode()

    projectile.remove()
  }

  public static onPlayerOverlapsMine(player: Player, mine: BlockMine): void {
    if (mine.canExplode())
      mine.setActive(true)
  }

  public static onParticleOverlapsMine(particle: Particle, mine: BlockMine): void {
    mine.explode()
  }

  public static onParticleOverlapsBarrel(particle: Particle, barrel: Barrel): void {
    barrel.decreaseHealth(particle.getDamages())

    particle.remove()
  }
}

const collisions: { [key: string]: { [key: string]: (e1: any, e2: any) => void } } = {
  [Player.name]: {
    [BlockDynamite.name]: Collision.onPlayerOverlapsMine,
    [BlockMine.name]: Collision.onPlayerOverlapsMine,
  },

  [Bullet.name]: {
    [Barrel.name]: Collision.onBulletOverlapsBarrel,
    [Crate.name]: Collision.onBulletOverlapsCrate,
    [BlockDynamite.name]: Collision.onProjectileOverlapsMine,
    [BlockMine.name]: Collision.onProjectileOverlapsMine,
    [Player.name]: Collision.onBulletOverlapsPlayer,
  },

  [Grenade.name]: {
    [BlockDynamite.name]: Collision.onProjectileOverlapsMine,
    [BlockMine.name]: Collision.onProjectileOverlapsMine,
    [Block.name]: Collision.onGrenadeOverlapsBlock,
  },

  [Rocket.name]: {
    [BlockDynamite.name]: Collision.onProjectileOverlapsMine,
    [BlockMine.name]: Collision.onProjectileOverlapsMine,
    [Barrel.name]: Collision.onRocketOverlapsBlock,
    [Crate.name]: Collision.onRocketOverlapsBlock,
    [Player.name]: Collision.onRocketOverlapsPlayer,
  },

  [SmokeGrenade.name]: {
    [BlockDynamite.name]: Collision.onProjectileOverlapsMine,
    [BlockMine.name]: Collision.onProjectileOverlapsMine,
    [Barrel.name]: Collision.onSmokeGrenadeOverlapsBlock,
    [Crate.name]: Collision.onSmokeGrenadeOverlapsBlock,
  },

  [Particle.name]: {
    [BlockDynamite.name]: Collision.onParticleOverlapsMine,
    [BlockMine.name]: Collision.onParticleOverlapsMine,
    [Barrel.name]: Collision.onParticleOverlapsBarrel,
  },
}

export function collide(e1: Entity, e2: Entity): void {
  const cases = [
    [e1, e2],
    [e2, e1],
  ]

  for (const [a1, a2] of cases) {
    if (a1.constructor.name in collisions) {
      if (a2.constructor.name in collisions[a1.constructor.name]) {
        collisions[a1.constructor.name][a2.constructor.name](a1, a2)

        return
      }
    }
  }
}
