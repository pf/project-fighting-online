import Circle from './Circle'
import Game from './Game'
import OSimplexNoise from './OSimplexNoise'
import Rect from './Rect'
import { rand as randSeed } from './Seeder'
import { clamp, drawImageByIndex, rand } from './Util'

export const MapWidth = 200
export const MapHeight = 200

export class Map {
  private walls: Array<Array<number>>
  private render: CanvasRenderingContext2D | null
  private noiseAngle: OSimplexNoise | null
  private noiseRadius: OSimplexNoise | null
  private seed: number

  public constructor() {
    this.walls = []
    this.render = null
    this.noiseAngle = null
    this.noiseRadius = null
    this.seed = randSeed(0, 100000)
  }

  public worm(
    cells: Uint8Array,
    w: number,
    h: number,
    x: number,
    y: number,
    minR: number,
    baseR: number,
    maxPath: number,
  ) {
    this.noiseAngle = new OSimplexNoise()
    this.noiseRadius = new OSimplexNoise()
    this.noiseAngle.setSeed(this.seed + h)
    this.noiseRadius.setSeed(this.seed + h)

    let dirX = 1
    let dirY = 1

    for (let i = 0, count2 = maxPath ; i < count2 ; i++) {
      const angle = this.noiseAngle.noise2D(i / 100, i / 100) * Math.PI * 2
      const radius = minR + (this.noiseRadius.noise2D(i / 500, i / 500) + 1) / 2 * baseR

      for (let j = 0, count1 = (2 * radius) ; j < count1 ; j++) {
        for (let k = 0, count = (2 * radius) ; k < count ; k++) {
          if (
            (Math.floor(j - radius) * Math.floor(j - radius)) > (radius * radius) ||
            (Math.floor(k - radius) * Math.floor(k - radius)) > (radius * radius)
          ) {
            continue
          }

          // y * width + x
          // cf. http://stackoverflow.com/a/2151141/1974228
          const xpos = Math.floor(x + j - radius)
          const ypos = Math.floor(y + k - radius)
          const index = Math.floor(xpos + ypos * w)
          cells[index] = 1
        }
      }

      x += Math.cos(angle) * dirX
      y += Math.sin(angle) * dirY

      if (x < 0 || x >= w)
        dirX = -dirX

      if (y < 0 || y >= h)
        dirY = -dirY

      x = clamp(x, 0, w - 1)
      y = clamp(y, 0, h - 1)
    }
  }

  // Génère la carte
  public generate(): void {
    const cells = new Uint8Array(MapWidth * MapHeight)

    this.worm(cells, MapWidth, MapHeight, 0, 0, 3, 5, 500)
    this.worm(cells, MapWidth, MapHeight, MapWidth, 0, 3, 5, 500)
    this.worm(cells, MapWidth, MapHeight, 0, MapHeight, 3, 5, 500)
    this.worm(cells, MapWidth, MapHeight, MapWidth, MapHeight, 3, 5, 500)

    for (let xx = 0 ; xx < MapWidth ; xx++) {
      cells[xx] = 0
      cells[(MapHeight - 1) * MapWidth + xx] = 0
    }

    for (let yy = 0 ; yy < MapHeight ; yy++) {
      cells[yy * MapWidth] = 0
      cells[(yy + 1) * MapWidth - 1] = 0
    }

    this.walls = []

    for (let yyy = 0 ; yyy < MapHeight ; yyy++)
      this.walls[yyy] = []

    let y = -1

    for (let i = 0 ; i < cells.length ; i++) {
      const x = i % MapWidth

      if (x === 0)
        y++

      const cell = cells[i]

      if (cell === 0)
        this.walls[y][x] = 0
      else
        this.walls[y][x] = -1
    }
  }

  public generateRender(game: Game): void {
    const tmpCan = document.createElement('canvas') as HTMLCanvasElement
    tmpCan.width = MapWidth * Game.TILE_SIZE
    tmpCan.height = MapHeight * Game.TILE_SIZE

    const level = tmpCan.getContext('2d')

    if (!level)
      return

    level.clearRect(0, 0, tmpCan.width, tmpCan.height)

    level.fillRect(20, 20, 40, 40)

    for (let y = 0 ; y < MapHeight ; y++) {
      for (let x = 0 ; x < MapWidth ; x++) {
        drawImageByIndex(
          level,
          game.getImageManager().getImage('ground'),
          x * Game.TILE_SIZE,
          y * Game.TILE_SIZE,
          rand(0, 7),
          Game.TILE_SIZE,
          Game.TILE_SIZE,
        )
      }
    }

    for (let y = 0 ; y < MapHeight ; y++) {
      for (let x = 0 ; x < MapWidth ; x++) {
        if (this.getWall(x, y) !== -1) {
          level.drawImage(
            game.getImageManager().getImage('wall'),
            x * Game.TILE_SIZE,
            y * Game.TILE_SIZE,
          )
        }
      }
    }

    this.render = level
  }

  public getRender(): CanvasRenderingContext2D | null {
    return this.render
  }

  public getWalls(): Array<Array<number>> {
    return this.walls
  }

  public getWall(x: number, y: number): number | null {
    const a = this.walls[y]

    if (a) {
      const b = a[x]

      if (b) {
        return b
      }
    }

    return null
  }

  public getSeed(): number {
    return this.seed
  }

  public setSeed(seed: number): this {
    this.seed = seed

    return this
  }

  // à faire : prendre en compte le rayon
  public getBlocksAround(circle: Circle): Array<Rect> {
    // Copie de la liste des blocs mais en gardant les références
    // const rects = this.walls.slice(0)

    const rects = new Array<Rect>()

    const pos = circle.getPosition()
    pos.div(Game.TILE_SIZE)
    pos.setX(Math.floor(pos.getX()))
    pos.setY(Math.floor(pos.getY()))

    for (let x = pos.getX() - 2 ; x < pos.getX() + 2 ; x++) {
      for (let y = pos.getY() - 2 ; y < pos.getY() + 2 ; y++) {
        if (this.getWall(x, y) !== -1) {
          const wall = new Rect(
            x * Game.TILE_SIZE,
            y * Game.TILE_SIZE,
            Game.TILE_SIZE,
            Game.TILE_SIZE,
          )

          rects.push(wall)
        }
      }
    }

    return rects
  }
}
