import Circle from './Circle'
import Point from './Point'

export default class Rect {
  private position: Point
  private width: number
  private height: number

  public constructor(x: number, y: number, width: number, height: number) {
    this.position = new Point(x, y)
    this.width = width
    this.height = height
  }

  // Récupérer la position du centre du rectangle
  public getCenter(): Point {
    return new Point(
      this.getX() + this.width / 2,
      this.getY() + this.height / 2,
    )
  }

  public getPosition(): Point {
    return this.position
  }

  public setPosition(position: Point): this {
    this.position.setPosition(position)

    return this
  }

  public setSize(width: number, height: number): this {
    this.setWidth(width)
    this.setHeight(height)

    return this
  }

  public getX(): number {
    return this.position.getX()
  }

  public setX(x: number): this {
    this.position.setX(x)

    return this
  }

  public getY(): number {
    return this.position.getY()
  }

  public setY(y: number): this {
    this.position.setY(y)

    return this
  }

  public getWidth(): number {
    return this.width
  }

  public setWidth(width: number): this {
    this.width = width

    return this
  }

  public getHeight(): number {
    return this.height
  }

  public setHeight(height: number): this {
    this.height = height

    return this
  }

  // Récupère le cercle correspondant
  public getCircle(): Circle {
    const position = this.getCenter()
    const radius = (this.width + this.height) / 4

    return new Circle(position.getX(), position.getY(), radius)
  }
}
