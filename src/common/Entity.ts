import Circle from './Circle'
import Rect from './Rect'

type Shape = Circle | Rect

export default interface Entity {
  getCollisionShape(): Shape
}
