export default class Point {
  public static angleBetween(p1: Point, p2: Point): number {
    return Math.atan2(p2.y - p1.y, p2.x - p1.x)
  }

  private x: number
  private y: number

  public constructor(x: number, y: number) {
    this.x = x
    this.y = y
  }

  public clone(): Point {
    return new Point(this.x, this.y)
  }

  public scale(n: number): this {
    this.x *= n
    this.y *= n

    return this
  }

  public div(n: number): this {
    this.x /= n
    this.y /= n

    return this
  }

  public add(p: Point): this {
    this.x += p.getX()
    this.y += p.getY()

    return this
  }

  public addX(x: number): this {
    this.x += x

    return this
  }

  public addY(y: number): this {
    this.y += y

    return this
  }

  public setPosition(position: Point): this {
    this.setX(position.x)
    this.setY(position.y)

    return this
  }

  public getX(): number {
    return this.x
  }

  public setX(x: number): this {
    this.x = x

    return this
  }

  public getY(): number {
    return this.y
  }

  public setY(y: number): this {
    this.y = y

    return this
  }

  /*

  public equals(p: Point): boolean {
    return p.x == this.x && p.y == this.y
  }

  public sub(p: Point): Point {
    const point = this.clone()
    point.x -= p.x
    point.y -= p.y

    return point
  }

  public getMagnitude(): number {
    return Math.sqrt(this.x * this.x + this.y * this.y)
  }

  public getNormalized(): Point {
    if (this.x === 0 && this.y === 0)
      return this.clone()

    return this.scale(1 / this.getMagnitude())
  }

  */
}
