// Génère un nombre aléatoire en fonction de la valeur de la graine.
//
// Permet de générer un même nombre aléatoire entre deux systèmes indépendants
// en fonction d'un même nombre (la graine).

// @see https://stackoverflow.com/a/19303725
let seederSeed = 1

// Aléatoire entre min et max inclus
export function rand(min: number, max: number): number {
  return Math.floor(random() * (max - min + 1)) + min
}

// Aléatoire entre 0 (inclus) et 1 (exclus)
export function random(): number {
  const x = Math.sin(seederSeed) * 10000

  return x - Math.floor(x)
}

// Définir la valeur de la graine
export function setSeed(seed: number): void {
  seederSeed = seed
}
