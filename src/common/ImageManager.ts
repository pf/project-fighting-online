// Gestion du chargement des images
//
// Fonctionnement :
//   const imageManager = new ImageManager()
//   imageManager.setPath('assets/img')
//   imageManager.setCallback(run)
//   imageManager.load(images)
export default class ImageManager {
  // Images chargées
  private images: { [key: string]: HTMLImageElement }

  // Chemin où sont recupérées les images
  private path: string

  // Fonction appelée une fois les images chargées
  private callback: (() => void) | null

  public constructor() {
    this.images = {}
    this.path = 'assets/img/'
    this.callback = null
  }

  // Obtenir une image par son nom
  public getImage(name: string): HTMLImageElement {
    return this.images[name]
  }

  // Définir le chemin dans lequel les images sont chargées (sans la barre
  // oblique (slash) finale).
  public setPath(path: string): void {
    this.path = path + '/'
  }

  // Définir la fonction appelée une fois les images chargées.
  public setCallback(callback: () => void): void {
    this.callback = callback
  }

  // Obtenir le chemin dans lequel les images sont chargées
  public getPath(): string {
    return this.path
  }

  // Exécute le chargement des images et lance la fonction callback une fois le
  // chargement terminé.
  // Note : si toutes les images ne sont pas chargées (par exemple une URL
  // incorrecte), la fonction callback n'est pas appelée.
  public load(images: { [key: string]: string }): void {
    let nbImagesToLoad = Object.keys(images).length

     // Évènement lorsqu'une image est chargée :
     // - décrémenter le compteur
     // - lancer le callback si le nombre d'images à charger est descendu à zéro
    const imageLoaded = () => {
      nbImagesToLoad -= 1

      if (nbImagesToLoad === 0)
        if (this.callback)
          this.callback()
    }

     // L'image n'a pas pu charger
    const imageError = (e: Event) => {
      const srcImageError = (e.target as HTMLImageElement).src
      throw new Error(`Impossible de charger l'image « ${srcImageError} »`)
    }

    // Chargement des images
    for (const [name, url] of Object.entries(images)) {
      const img = document.createElement('img') as HTMLImageElement
      img.addEventListener('load', imageLoaded)
      img.addEventListener('error', imageError)
      img.src = this.getPath() + url
      img.name = name

      this.images[name] = img
    }
  }
}
