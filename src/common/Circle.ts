import Point from './Point'

export default class Circle {
  private position: Point
  private radius: number

  public constructor(x: number, y: number, radius: number) {
    this.position = new Point(x, y)
    this.radius = radius
  }

  public getPosition(): Point {
    return this.position
  }

  public setPosition(position: Point): this {
    this.position.setPosition(position)

    return this
  }

  public getRadius(): number {
    return this.radius
  }

  public setRadius(radius: number): this {
    this.radius = radius

    return this
  }

  public getX(): number {
    return this.position.getX()
  }

  public setX(x: number): this {
    this.position.setX(x)

    return this
  }

  public getY(): number {
    return this.position.getY()
  }

  public setY(y: number): this {
    this.position.setY(y)

    return this
  }
}
