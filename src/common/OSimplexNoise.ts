const STRETCH_CONSTANT_2D = -0.211324865405187118 // (1/sqrt(2+1)-1)/2
const SQUISH_CONSTANT_2D = 0.366025403784438647 // (sqrt(2+1)-1)/2
const NORM_CONSTANT_2D = 47

export default class OSimplexNoise {
  private seed: number | null
  private randArray: Int8Array

  constructor() {
    this.seed = null
    this.randArray = new Int8Array(256)
  }

  public setSeed(seed: number | null = null): void {
    this.seed = seed

    if (seed === null) {
      for (let j = 0 ; j < 256 ; j++)
        this.randArray[j] = Math.random() * 256

      return
    }

    const source = new Int8Array(256)

    for (let k = 0 ; k < 256 ; k++)
      source[k] = k

    const a = 6
    const b = 1

    seed *= a + b
    seed *= a + b
    seed *= a + b

    let i = 255
    while (true) {
      let r: number

      seed *= a + b
      r = (seed + 31) % (i + 1)

      if (r < 0) {
        r += i + 1
      }

      this.randArray[i] = source[i]
      source[r] = source[i]

      if (i === 0)
        break

      i--
    }
  }

  public getSeed(): number | null {
    return this.seed
  }

  public noise2D(x: number, y: number): number {
    x = +x
    y = +y

    const stretchOffset = (x + y) * STRETCH_CONSTANT_2D
    const xs = x + stretchOffset
    const ys = y + stretchOffset

    let xsb = Math.floor(xs)
    let ysb = Math.floor(ys)

    const squishOffset = (xsb + ysb) * SQUISH_CONSTANT_2D
    const xb = xsb + squishOffset
    const yb = ysb + squishOffset

    const xins = xs - xsb
    const yins = ys - ysb

    const inSum = xins + yins

    let dx0 = x - xb
    let dy0 = y - yb

    let dxExt = 0
    let dyExt = 0

    let xsvExt = 0
    let ysvExt = 0

    let value = 0

    const dx1 = dx0 - 1 - SQUISH_CONSTANT_2D
    const dy1 = dy0 - 0 - SQUISH_CONSTANT_2D

    let attn1 = 2 - dx1 * dx1 - dy1 * dy1

    if (attn1 > 0) {
      attn1 *= attn1
      value += attn1 * attn1 * this.extrapolate2D(Math.floor(xsb + 1), Math.floor(ysb + 0), dx1, dy1)
    }

    const dx2 = dx0 - 0 - SQUISH_CONSTANT_2D
    const dy2 = dy0 - 1 - SQUISH_CONSTANT_2D
    let attn2 = 2 - dx2 * dx2 - dy2 * dy2

    if (attn2 > 0) {
      attn2 *= attn2
      value += attn2 * attn2 * this.extrapolate2D(Math.floor(xsb + 0), Math.floor(ysb + 1), dx2, dy2)
    }

    if (inSum <= 1) {
      const zins = 1 - inSum

      if (zins > xins || zins > yins) {
        if (xins > yins) {
          xsvExt = xsb + 1
          ysvExt = ysb - 1
          dxExt = dx0 - 1
          dyExt = dy0 + 1
        } else {
          xsvExt = xsb - 1
          ysvExt = ysb + 1
          dxExt = dx0 + 1
          dyExt = dy0 - 1
        }
      } else {
        xsvExt = xsb + 1
        ysvExt = ysb + 1
        dxExt = dx0 - 1 - 2 * SQUISH_CONSTANT_2D
        dyExt = dy0 - 1 - 2 * SQUISH_CONSTANT_2D
      }
    } else {
      const zins1 = 2 - inSum

      if (zins1 < xins || zins1 < yins) {
        if (xins > yins) {
          xsvExt = xsb + 2
          ysvExt = ysb + 0
          dxExt = dx0 - 2 - 2 * SQUISH_CONSTANT_2D
          dyExt = dy0 + 0 - 2 * SQUISH_CONSTANT_2D
        } else {
          xsvExt = xsb + 0
          ysvExt = ysb + 2
          dxExt = dx0 + 0 - 2 * SQUISH_CONSTANT_2D
          dyExt = dy0 - 2 - 2 * SQUISH_CONSTANT_2D
        }
      } else {
        dxExt = dx0
        dyExt = dy0
        xsvExt = xsb
        ysvExt = ysb
      }

      xsb = xsb + 1
      ysb = ysb + 1
      dx0 = dx0 - 1 - 2 * SQUISH_CONSTANT_2D
      dy0 = dy0 - 1 - 2 * SQUISH_CONSTANT_2D
    }

    let attn0 = 2 - dx0 * dx0 - dy0 * dy0

    if (attn0 > 0) {
      attn0 *= attn0
      value += attn0 * attn0 * this.extrapolate2D(xsb, ysb, dx0, dy0)
    }

    let attnExt = 2 - dxExt * dxExt - dyExt * dyExt

    if (attnExt > 0) {
      attnExt *= attnExt
      value += attnExt * attnExt * this.extrapolate2D(xsvExt, ysvExt, dxExt, dyExt)
    }

    return value / NORM_CONSTANT_2D
  }

  private extrapolate2D(xsb: number, ysb: number, dx: number, dy: number): number {
    const gradients = new Int8Array([5, 2, 2, 5, -5, 2, -2, 5, 5, -2, 2, -5, -5, -2, -2, -5])
    const idx = this.randArray[this.randArray[xsb & 255] + ysb & 255] & 14

    return gradients[idx] * dx + gradients[idx + 1] * dy
  }
}
