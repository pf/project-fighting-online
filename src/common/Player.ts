import Weapon from '../common/weapon/Weapon'
import Animation from './Animation'
import AnimationManager from './AnimationManager'
import Circle from './Circle'
import Entity from './Entity'
import { PlayerEvent } from './Event'
import Game from './Game'
import { FirePacket } from './Packet'
import Point from './Point'
import Dynamite from './weapon/Dynamite'
import Flamethrower from './weapon/Flamethrower'
import Grenade from './weapon/Grenade'
import Gun from './weapon/Gun'
import Mine from './weapon/Mine'
import RocketLauncher from './weapon/RocketLauncher'
import Shotgun from './weapon/Shotgun'
import SmokeGrenade from './weapon/SmokeGrenade'
import SubmachineGun from './weapon/SubmachineGun'

type Direction = 'front' | 'back' | 'side-right' | 'side-left'

const PLAYER_SPEED = 200

const COLORS = [
  'red', 'green', 'blue', 'purple', 'pink',
  'brown', 'indigo', 'black', 'magenta',
  'cyan', 'orange', ' gray',
]

const SLIDE_MAGIC_NUMBER = 0.1

const CLOSE_TO_ZERO = 0.01

const BASE_HEALTH = 100

export default class Player implements Entity {
  private game: Game

  private targetPosition: Point
  private position: Point

  private width: number
  private height: number

  private velocity: Point

  private health: number
  private maxHealth: number

  private angle: number

  private uuid: string

  private color: string

  private weapons: Array<Weapon>
  private currentWeaponIndex: number

  private animations: AnimationManager

  private previousDir: Direction
  private dir: Direction

  public constructor(game: Game) {
    this.game = game

    this.position = new Point(48, 48)
    this.targetPosition = new Point(this.position.getX(), this.position.getY())

    this.width = 14
    this.height = 15

    this.velocity = new Point(0, 0)

    this.health = BASE_HEALTH
    this.maxHealth = BASE_HEALTH

    this.angle = 0

    this.uuid = ''

    this.color = COLORS[Math.floor(Math.random() * COLORS.length)]

    this.weapons = [
      new Dynamite(game),
      new Gun(game),
      new Mine(game),
      new Grenade(game),
      new RocketLauncher(game),
      new SmokeGrenade(game),
      new SubmachineGun(game),
      new Shotgun(game),
      new Flamethrower(game),
    ]
    this.currentWeaponIndex = 0

    this.animations = new AnimationManager(
      this.getGame().getImageManager().getImage('player'),
      14,
      15,
    )

    this.addAnimation('idle-front',      [ 0 ])
    this.addAnimation('idle-back',       [ 8 ])
    this.addAnimation('idle-side-right', [ 12 ])
    this.addAnimation('idle-side-left',  [ 4 ])

    this.addAnimation('walk-front',      [ 0, 1, 2, 3 ])
    this.addAnimation('walk-back',       [ 8, 9, 10, 11 ])
    this.addAnimation('walk-side-right', [ 12, 13, 14, 15 ])
    this.addAnimation('walk-side-left',  [ 4, 5, 6, 7 ])

    this.play('idle-front')

    this.previousDir = 'front'
    this.dir = 'front'
  }

  public getMiddle(): Point {
    return new Point(
      this.width / 2,
      this.height / 2,
    )
  }

  public getCenter(): Point {
    const middle = this.getMiddle()

    return new Point(
      this.position.getX() + middle.getX(),
      this.position.getY() + middle.getY(),
    )
  }

  public getPosition(): Point {
    return this.position
  }

  public setPosition(position: Point): this {
    this.position.setPosition(position)

    return this
  }

  public getX(): number {
    return this.position.getX()
  }

  public setX(x: number): this {
    this.position.setX(x)

    return this
  }

  public getY(): number {
    return this.position.getY()
  }

  public setY(y: number): this {
    this.position.setY(y)

    return this
  }

  public getWidth(): number {
    return this.width
  }

  public getHeight(): number {
    return this.height
  }

  // Récupère le cercle correspondant
  public getCircle(): Circle {
    const position = this.getCenter()
    const radius = (this.width + this.height) / 4

    return new Circle(position.getX(), position.getY(), radius)
  }

  public getCollisionShape() {
    return this.getCircle()
  }

  /*

  public render(ctx: CanvasRenderingContext2D): void {
    const middle = this.getMiddle()

    ctx.save()

    ctx.translate(this.position.getX(), this.position.getY())
    ctx.translate(middle.getX(), middle.getY())

    if (this.angle != 0)
      ctx.rotate(this.angle)

    ctx.translate(-middle.getX(), -middle.getY())

    if (this.animations != null) {
      this.animations.render(ctx)
    } else {
      ctx.fillStyle = 'rgb(0, 0, 0)'
      ctx.fillRect(0, 0, this.width, this.height)
    }

    ctx.restore()
  }

  */

  // Définir la vitesse
  public setVelocity(velocity: Point): this {
    this.velocity = velocity

    return this
  }

  // Obtenir la position du sprite par rapport à la grille du jeu
  public getTilePosition(): Array<number> {
    return [
      this.position.getX() / Game.TILE_SIZE,
      this.position.getY() / Game.TILE_SIZE,
    ]
  }

  public setHealth(health: number): this {
    this.health = health

    return this
  }

  public setMaxHealth(maxHealth: number): this {
    this.maxHealth = maxHealth

    return this
  }

  public getHealth(): number {
    return this.health
  }

  public getMaxHealth(): number {
    return this.maxHealth
  }

  // Le joueur est-il mort ?
  public isDead(): boolean {
    return this.health <= 0
  }

  // Obtention du pourcentage de vie restant
  public getHealthPercentage(): number {
    return this.health / this.maxHealth
  }

  // Baisser la vie
  public decreaseHealth(value: number): void {
    this.health -= value

    if (this.health < 0)
      this.health = 0
  }

  // Monter la vie
  public increaseHealth(value: number): void {
    this.health += value

    if (this.health > this.maxHealth)
      this.health = this.maxHealth
  }

  // Réiniatiser la vie
  public resetHealth(): void {
    this.health = this.maxHealth
  }

  // Tue le sprite
  public kill(): void {
    this.health = 0
  }

  // Le sprite peut-il être supprimé ?
  public canBeRemoved(): boolean {
    return this.health <= 0
  }

  public getVelocity(): Point {
    return this.velocity
  }

  public getAngle(): number {
    return this.angle
  }

  public setAngle(angle: number): this {
    this.angle = angle

    return this
  }

  public getGame(): Game {
    return this.game
  }

  public update(dt: number): void {
    this.targetPosition.addX(this.velocity.getX() * dt)
    this.targetPosition.addY(this.velocity.getY() * dt)

    const newX = lerp(this.position.getX(), this.targetPosition.getX(), SLIDE_MAGIC_NUMBER)
    const nexY = lerp(this.position.getY(), this.targetPosition.getY(), SLIDE_MAGIC_NUMBER)

    this.position.setX(newX)
    this.position.setY(nexY)

    if (Math.abs(this.targetPosition.getX() - this.position.getX()) < CLOSE_TO_ZERO)
      this.position.setX(this.targetPosition.getX())

    if (Math.abs(this.targetPosition.getY() - this.position.getY()) < CLOSE_TO_ZERO)
      this.position.setY(this.targetPosition.getY())

    this.updateDir(dt)

    const currentAnimation = this.getCurrentAnimation()

    if (currentAnimation !== null)
      currentAnimation.update(dt)

    for (const weapon of this.weapons)
      weapon.update(dt)
  }

  public getWeapons(): Array<Weapon> {
    return this.weapons
  }

  public getCurrentWeapon(): Weapon {
    return this.weapons[this.currentWeaponIndex]
  }

  public getUuid(): string {
    return this.uuid
  }

  public setUuid(uuid: string): this {
    this.uuid = uuid

    return this
  }

  public getTargetPosition(): Point {
    return this.targetPosition
  }

  public setTargetPosition(targetPosition: Point): this {
    this.targetPosition = targetPosition

    return this
  }

  public getColor(): string {
    return this.color
  }

  public setColor(color: string): this {
    this.color = color

    return this
  }

  public fire(mousePosition: Point): void {
    const center = this.getCenter()
    const angle = Point.angleBetween(center, mousePosition)

    const currentWeapon = this.getCurrentWeapon()

    const hasFire = currentWeapon.fire(center, angle)

    if (hasFire) {
      this.game.getSocket().emit(PlayerEvent.Fire, {
        weapon: currentWeapon.getType(),
        x: center.getX(),
        y: center.getY(),
        ts: Date.now(),
        angle: angle,
        speed: 0,
        seed: 0,
      } as FirePacket)
    }
  }

  public getCurrentWeaponIndex(): number {
    return this.currentWeaponIndex
  }

  public setCurrentWeaponIndex(currentWeaponIndex: number): this {
    this.currentWeaponIndex = currentWeaponIndex

    return this
  }

  public move(direction: Point): void {
    const zero = new Point(0, 0)

    if (direction.getX() === 0 && direction.getY() === 0) {
      this.setVelocity(zero)

      return
    }

    const angle = Point.angleBetween(zero, direction)

    const velocity = new Point(
      Math.cos(angle),
      Math.sin(angle),
    )

    velocity.scale(PLAYER_SPEED)

    this.setVelocity(velocity)
  }

  // Charge une image
  public setImage(
    image: HTMLImageElement,
    width: number | null = null,
    height: number | null = null,
  ) {
    if (!width)
      width = image.width

    if (!height)
      height = image.height

    this.animations = new AnimationManager(image, width, height)
  }

  // Ajoute une animation
  public addAnimation(name: string, frames: Array<number>): void {
    this.animations.addAnimation(name, frames)
  }
  // Obtenir l'animation courante
  public getCurrentAnimation(): Animation | null {
    return this.animations.getCurrentAnimation()
  }

  // Jouer une animation
  public play(name: string, force: boolean = false): void {
    const nextAnimation = this.animations.getAnimation(name)

    if ((this.animations.getCurrentAnimation() !== nextAnimation || force) && nextAnimation)
      this.animations.setCurrentAnimation(nextAnimation)
  }

  public render(ctx: CanvasRenderingContext2D): void {
    const center = this.getCenter()

    ctx.textAlign = 'center'
    ctx.fillStyle = this.getColor()
    ctx.fillText(this.getColor(), center.getX(), center.getY() + this.getHeight() + 2)

    ctx.save()

    ctx.translate(center.getX(), center.getY())
    ctx.translate(-this.getWidth() / 2, -this.getHeight() / 2)

    const currentAnimation = this.getCurrentAnimation()

    if (currentAnimation !== null)
      currentAnimation.render(ctx)

    ctx.restore()
  }

  protected updateDir(dt: number): void {
    const angle = this.getAngle()

    let dir: Direction = 'front'

    if (angle >= 0.25 && angle < 0.75)
      dir = 'front'
    else if (angle >= -0.75 && angle < -0.25)
      dir = 'back'
    else if (angle >= -0.25 && angle < 0.25)
      dir = 'side-right'
    else if ((angle >= -1 && angle < -0.75) || (angle > 0.75 && angle < 1))
      dir = 'side-left'

    const inMovement =
         Math.abs(this.getPosition().getX() - this.getTargetPosition().getX()) > 2
      || Math.abs(this.getPosition().getY() - this.getTargetPosition().getY()) > 2

    if (inMovement)
      this.play(`walk-${dir}`)
    else
      this.play(`idle-${dir}`)

    this.dir = dir
  }

  protected getDir(): Direction {
    return this.dir
  }

  protected getPreviousDir(): Direction {
    return this.previousDir
  }

  protected setPreviousDir(previousDir: Direction): this {
    this.previousDir = previousDir

    return this
  }
}

function lerp(v0: number, v1: number, t: number): number {
  return v0 * (1 - t) + v1 * t
}
