export const PlayerEvent = {
  Joined: 'player:joined',
  Quit: 'player:quit',
  // Coordinates: 'player:coordinates',
  Fire: 'player:fire',
  ChangeAngle: 'player:angle',
  ChangePosition: 'player:position',
}
