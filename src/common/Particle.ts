import Circle from './Circle'
import Entity from './Entity'
import Game from './Game'
import Player from './Player'
import Point from './Point'
import { rand } from './Util'

/*

const colors = [
  'rgb(255, 50, 8)',
  'rgb(232, 98, 14)',
  'rgb(240, 188, 37)',
  'rgb(35, 117, 179)',
]

*/

// Converti des degrés en radians
function degreesToRadians(degrees: number): number {
  return degrees * (Math.PI / 180)
}

// Converti des radians en degrés
function radiansToDegrees(radians: number): number {
  return radians * (180 / Math.PI)
}

function randomElement<T>(items: Array<T>): T {
  return items[Math.floor(Math.random() * items.length)]
}

export default class Particle implements Entity {
  private game: Game

  private position: Point
  private velocity: Point

  private lifetime: number
  private maxLifetime: number

  private radius: number
  private maxRadius: number

  private speed: number

  private color: string

  private damages: number

  // Créer une particule
  public constructor(game: Game) {
    this.game = game

    this.position = new Point(0, 0)
    this.velocity = new Point(0, 0)

    this.lifetime = 0
    this.maxLifetime = 0

    this.radius = 0
    this.maxRadius = 0

    this.speed = 0

    this.color = 'rgb(255, 255, 255)'

    this.damages = 0
  }

  // Mise à jour d'une particule
  public update(dt: number): void {
    this.lifetime -= dt

    if (this.lifetime > 0) {
      const ageRatio = this.lifetime / this.maxLifetime

      this.radius = this.maxRadius * ageRatio

      this.position.addX(this.velocity.getX() * dt)
      this.position.addY(this.velocity.getY() * dt)
    } else {
      this.game.removeParticle(this)
    }
  }

  // Affichage d'une particule
  public render(ctx: CanvasRenderingContext2D): void {
    ctx.beginPath()
    ctx.arc(
      this.position.getX(),
      this.position.getY(),
      this.radius,
      0,
      2 * Math.PI,
      false,
    )
    ctx.fillStyle = this.color
    ctx.fill()

    // ctx.drawPixelatedCircle(this.m_position.x, this.m_position.y, this.m_radius)
  }

  public setPosition(position: Point): this {
    this.position = position

    return this
  }

  public setSpeed(min: number, max: number): this {
    this.speed = rand(min, max)

    return this
  }

  public setLifetime(min: number, max: number): this {
    this.maxLifetime = rand(min, max)
    this.lifetime = this.maxLifetime

    return this
  }

  public setRadius(min: number, max: number): this {
    this.maxRadius = rand(min, max)
    this.radius = this.maxRadius

    return this
  }

  public setAngle(min: number, max: number): this {
    const angle = rand(min, max)
    const angleInRadians = degreesToRadians(angle)

    this.velocity = new Point(
      this.speed * Math.cos(angleInRadians),
      -this.speed * Math.sin(angleInRadians),
    )

    return this
  }

  public setColor(colors: Array<string>): this {
    this.color = randomElement(colors)

    return this
  }

  public setDamages(damages: number): this {
    this.damages = damages

    return this
  }

  public isAlive(): boolean {
    return this.lifetime > 0
  }

  public onPlayerOverlaps(player: Player): void {
    if (!this.damages)
      return

    player.decreaseHealth(this.damages)

    this.remove()
  }

  public getCircle(): Circle {
    const radius = this.radius / 2

    return new Circle(this.position.getX(), this.position.getY(), radius)
  }

  public getCollisionShape() {
    return this.getCircle()
  }

  public getDamages(): number {
    return this.damages
  }

  public remove(): void {
    this.game.removeParticle(this)
  }
}
