import Block from './block/Block'
import { collide } from './Collision'
import Hit from './Hit'
import ImageManager from './ImageManager'
import { Map } from './Map'
import Particle from './Particle'
import Player from './Player'
import Projectile from './weapon/projectile/Projectile'

const WIDTH = 640
const HEIGHT = 480

export const TILE_SIZE = 16

export const RELEASE = false

export default class Game {
  static get WIDTH() { return WIDTH }
  static get HEIGHT() { return HEIGHT }
  static get TILE_SIZE() { return TILE_SIZE }
  static get RELEASE() { return RELEASE }

  private players: { [uuid: string]: Player }
  private projectiles: Array<Projectile>
  private blocks: Array<Block>
  private particles: Array<Particle>
  private lastUpdate: number
  private imageManager: ImageManager
  private map: Map
  private socket: any
  private dt: number

  public constructor() {
    this.players = {}
    this.projectiles = []
    this.blocks = []
    this.particles = []

    this.lastUpdate = Date.now()
    this.dt = 0

    this.imageManager = new ImageManager()

    this.map = new Map()
    this.map.generate()
  }

  public addPlayer(player: Player): void {
    this.players[player.getUuid()] = player
  }

  public removePlayer(player: Player): void {
    if (typeof player !== 'undefined')
      delete this.players[player.getUuid()]
  }

  public getPlayer(uuid: string): Player {
    return this.players[uuid]
  }

  public addProjectile(projectile: Projectile): void {
    this.projectiles.push(projectile)
  }

  public removeProjectile(projectile: Projectile): void {
    this.projectiles = this.projectiles.filter(item => item !== projectile)
  }

  public removeParticle(particle: Particle): void {
    this.particles = this.particles.filter(item => item !== particle)
  }

  public addParticle(particle: Particle): void {
    this.particles.push(particle)
  }

  public addBlock(block: Block): void {
    this.blocks.push(block)
  }

  public removeBlock(block: Block): void {
    this.blocks = this.blocks.filter(item => item !== block)
  }

  public start(): void {
    this.update()
  }

  public update(): void {
    const now = Date.now()
    this.dt = (now - this.lastUpdate) / 1000
    this.lastUpdate = now

    for (const i in this.players) {
      if (!this.players.hasOwnProperty(i))
        continue

      const player = this.players[i]
      player.update(this.dt)
    }

    for (const projectile of this.projectiles)
      projectile.update(this.dt)

    for (const block of this.blocks)
      block.update(this.dt)

    for (const particle of this.particles)
      particle.update(this.dt)

    const players = this.getPlayers()

    for (const i in players) {
      if (!players.hasOwnProperty(i))
        continue

      const player = players[i]

      for (const projectile of this.getProjectiles()) {
        const collision = Hit.circleHitCircle(
          player.getCircle(),
          projectile.getCircle(),
        )

        if (collision)
          collide(player, projectile)
      }

      for (const block of this.getBlocks()) {
        const collision = Hit.circleHitCircle(
          player.getCircle(),
          block.getCircle(),
        )

        if (collision)
          collide(block, player)
      }

      for (const particle of this.getParticles()) {
        const collision = Hit.circleHitCircle(
          player.getCircle(),
          particle.getCircle(),
        )

        if (collision)
          particle.onPlayerOverlaps(player)
      }
    }

    for (const projectile of this.getProjectiles()) {
      for (const block of this.getBlocks()) {
        const collision = Hit.circleHitCircle(
          block.getCircle(),
          projectile.getCircle(),
        )

        if (collision)
          collide(block, projectile)
      }
    }

    for (const particle of this.getParticles()) {
      for (const block of this.getBlocks()) {
        const collision = Hit.circleHitCircle(
          particle.getCollisionShape(),
          block.getCollisionShape(),
        )

        if (collision)
          collide(particle, block)
      }
    }
  }

  public getImageManager(): ImageManager {
    return this.imageManager
  }

  public getSocket(): any {
    return this.socket
  }

  public getMap(): Map {
    return this.map
  }

  protected getPlayers(): { [key: string]: Player } {
    return this.players
  }

  protected getProjectiles(): Array<Projectile> {
    return this.projectiles
  }

  protected getBlocks(): Array<Block> {
    return this.blocks
  }

  protected getParticles(): Array<Particle> {
    return this.particles
  }

  protected setSocket(socket: any | null): this {
    this.socket = socket

    return this
  }

  protected getDt(): number {
    return this.dt
  }
}
