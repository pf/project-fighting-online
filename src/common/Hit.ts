import Circle from '../common/Circle'
import Point from '../common/Point'
import Rect from '../common/Rect'

// Collisions
export default class Hit {
  // Point <-> Rect
  public static pointHitRect(p: Point, r: Rect): boolean {
    return (r.getX() <= p.getX() && p.getX() <= r.getX() + r.getWidth()) &&
      (r.getY() <= p.getY() && p.getY() <= r.getY() + r.getWidth())
  }

  // Point <-> Circle
  public static pointHitCircle(p: Point, c: Circle): boolean {
    return (p.getX() - c.getX()) ** 2 + (p.getY() - c.getY()) ** 2 < c.getRadius() ** 2
  }

  // Rect <-> Rect
  public static rectHitRect(r1: Rect, r2: Rect): boolean {
    return r1.getX() < r2.getX() + r2.getWidth() &&
      r1.getX() + r1.getWidth() > r2.getX() &&
      r1.getY() < r2.getY() + r2.getHeight() &&
      r1.getY() + r1.getHeight() > r2.getY()
  }

  // Circle <-> Circle
  // @see https://stackoverflow.com/a/8331359
  public static circleHitCircle(c1: Circle, c2: Circle): boolean {
    const r = c1.getRadius() + c2.getRadius()
    const x = c1.getX() - c2.getX()
    const y = c1.getY() - c2.getY()

    return r > Math.sqrt(x * x + y * y)
  }
}
