import Game from '../common/Game'
import { BlockName } from './block/BlockName'
import Point from './Point'
import Dynamite from './weapon/Dynamite'
import Flamethrower from './weapon/Flamethrower'
import Grenade from './weapon/Grenade'
import Gun from './weapon/Gun'
import Mine from './weapon/Mine'
import { ProjectileName } from './weapon/projectile/ProjectileName'
import RocketLauncher from './weapon/RocketLauncher'
import Shotgun from './weapon/Shotgun'
import SmokeGrenade from './weapon/SmokeGrenade'
import SubmachineGun from './weapon/SubmachineGun'
import Weapon from './weapon/Weapon'
import WeaponName from './weapon/WeaponName'

/*

type NewPlayerPacket = {
  x: number,
  y: number,
  color: number,
  uuid: string,
  yours: boolean,
  players: number,
  blocks: number,
}

*/

// Paquet générique envoyé au moment d'un tir (ou d'une pose de mine ou de
// dynamite)
export interface FirePacket {
  // Type d'arme utilisé
  weapon: WeaponName

  // Position
  x: number
  y: number

  // Timestamp au moment de l'envoi du paquet (en ms)
  ts: number

  // Angle et vitesse (facultatifs), utilisés pour calculer la vélocité s'il
  // s'agit d'un projectile en mouvement
  angle: number
  speed: number

  // Graine (facultative), utilisée pour générer des particules identiques chez
  // tous les joueurs
  seed: number
}

export interface PlayerAnglePacket {
  uuid: string
  angle: number
}

export interface PlayerPositionPacket {
  uuid: string
  x: number
  y: number
}

export interface BlockPacket {
  block: BlockName

  x: number
  y: number

  ts: number
}

export interface ProjectilePacket {
  projectile: ProjectileName

  x: number
  y: number

  ts: number
}

export function firePacketToWeapon(firePacket: FirePacket, game: Game): Weapon {
  let weapon: Weapon

  switch (firePacket.weapon) {
    case 'dynamite':
      weapon = new Dynamite(game)
      break

    case 'flamethrower':
      weapon = new Flamethrower(game)
      break

    case 'grenade':
      weapon = new Grenade(game)
      break

    case 'gun':
      weapon = new Gun(game)
      break

    case 'mine':
      weapon = new Mine(game)
      break

    case 'rocket-launcher':
      weapon = new RocketLauncher(game)
      break

    case 'shotgun':
      weapon = new Shotgun(game)
      break

    case 'smoke-grenade':
      weapon = new SmokeGrenade(game)
      break

    case 'submachine-gun':
      weapon = new SubmachineGun(game)
      break

    // @todo Cas à traiter
    default:
      weapon = new Gun(game)
      break
  }

  const position = new Point(firePacket.x, firePacket.y)
  const angle = firePacket.angle

  weapon.fire(position, angle)

  return weapon
}
