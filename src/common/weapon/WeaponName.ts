type WeaponName =
  | 'dynamite'
  | 'flamethrower'
  | 'grenade'
  | 'gun'
  | 'mine'
  | 'rocket-launcher'
  | 'shotgun'
  | 'smoke-grenade'
  | 'submachine-gun'

export default WeaponName
