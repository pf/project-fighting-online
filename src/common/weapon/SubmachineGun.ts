import Game from '../Game'
import Point from '../Point'
import Bullet from './projectile/Bullet'
import Weapon from './Weapon'
import WeaponName from './WeaponName'

export default class SubmachineGun extends Weapon {
  constructor(game: Game) {
    super(game, 'Mitraillette')

    this.setBaseLatency(0.1)
    this.setMiniatureIndex(6)
  }

  public getType(): WeaponName {
    return 'submachine-gun'
  }

  public fire(position: Point, angle: number): boolean {
    if (!this.canFire())
      return false

    this.decreaseAmmo()
    this.resetLatency()

    const bullet = new Bullet(this.getGame())
    bullet.fire(position, angle)

    return true
  }
}
