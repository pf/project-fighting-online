import Game from '../Game'
import Point from '../Point'
import Bullet from './projectile/Bullet'
import Weapon from './Weapon'
import WeaponName from './WeaponName'

export default class Shotgun extends Weapon {
  constructor(game: Game) {
    super(game, 'Fusil à pompe')

    this.setBaseLatency(1)
    this.setMiniatureIndex(5)
  }

  public getType(): WeaponName {
    return 'shotgun'
  }

  public fire(position: Point, angle: number): boolean {
    if (!this.canFire())
      return false

    this.decreaseAmmo()
    this.resetLatency()

    const bullet1 = new Bullet(this.getGame())
    bullet1.fire(position.clone(), angle - 0.2)

    const bullet2 = new Bullet(this.getGame())
    bullet2.fire(position.clone(), angle - 0.1)

    const bullet3 = new Bullet(this.getGame())
    bullet3.fire(position.clone(), angle)

    const bullet4 = new Bullet(this.getGame())
    bullet4.fire(position.clone(), angle + 0.1)

    const bullet5 = new Bullet(this.getGame())
    bullet5.fire(position.clone(), angle + 0.2)

    return true
  }
}
