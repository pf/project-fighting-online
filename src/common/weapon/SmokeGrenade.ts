import Game from '../Game'
import Point from '../Point'
import SmokeGrenadeBullet from './projectile/SmokeGrenade'
import Weapon from './Weapon'
import WeaponName from './WeaponName'

export default class SmokeGrenade extends Weapon {
  constructor(game: Game) {
    super(game, 'Fumigène')

    this.setBaseLatency(2)
    this.setMiniatureIndex(8)
  }

  public getType(): WeaponName {
    return 'smoke-grenade'
  }

  public fire(position: Point, angle: number): boolean {
    if (!this.canFire())
      return false

    this.decreaseAmmo()
    this.resetLatency()

    const smokeGrenade = new SmokeGrenadeBullet(this.getGame())
    smokeGrenade.fire(position, angle)

    return true
  }
}
