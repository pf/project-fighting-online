import MineBlock from '../block/Mine'
import Game from '../Game'
import Point from '../Point'
import Weapon from './Weapon'
import WeaponName from './WeaponName'

export default class Mine extends Weapon {
  constructor(game: Game) {
    super(game, 'Mine')

    this.setBaseLatency(0.5)
    this.setMiniatureIndex(3)
  }

  public getType(): WeaponName {
    return 'mine'
  }

  public fire(position: Point, angle: number): boolean {
    if (!this.canFire())
      return false

    this.decreaseAmmo()
    this.resetLatency()

    const mine = new MineBlock(this.getGame(), position.getX(), position.getY())
    this.getGame().addBlock(mine)

    return true
  }
}
