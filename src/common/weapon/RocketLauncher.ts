import Game from '../Game'
import Point from '../Point'
import Rocket from './projectile/Rocket'
import Weapon from './Weapon'
import WeaponName from './WeaponName'

export default class RocketLauncher extends Weapon {
  constructor(game: Game) {
    super(game, 'Bazooka')

    this.setBaseLatency(2)
    this.setMiniatureIndex(4)
  }

  public getType(): WeaponName {
    return 'rocket-launcher'
  }

  public fire(position: Point, angle: number): boolean {
    if (!this.canFire())
      return false

    this.decreaseAmmo()
    this.resetLatency()

    const rocket = new Rocket(this.getGame())
    rocket.fire(position, angle)

    return true
  }
}
