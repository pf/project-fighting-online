import Game from '../Game'
import Particle from '../Particle'
import Point from '../Point'
import { radiansToDegrees } from '../Util'
import Weapon from './Weapon'
import WeaponName from './WeaponName'

export default class Flamethrower extends Weapon {
  constructor(game: Game) {
    super(game, 'Lance-flammes')

    this.setMiniatureIndex(7)
  }

  public getType(): WeaponName {
    return 'flamethrower'
  }

  public fire(position: Point, angle: number): boolean {
    angle = -radiansToDegrees(angle)

    for (let i = 0 ; i < 3 ; i++) {
      const particle = new Particle(this.getGame())
        .setPosition(position.clone())
        .setSpeed(180, 220)
        .setLifetime(0.4, 0.6)
        .setRadius(3, 6)
        .setAngle(angle - 30, angle + 30)
        .setColor([
          'rgb(255, 0, 0)',
          'rgb(255, 140, 0)',
          'rgb(255, 215, 0)',
        ])
        .setDamages(3)

      // @todo Même solution magique que Projectile
      particle.update(0.08)

      this.getGame().addParticle(particle)
    }

    return true
  }
}
