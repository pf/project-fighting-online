import Game from '../Game'
import Point from '../Point'
import { drawImageByIndex } from '../Util'
import WeaponName from './WeaponName'

const MINIATURE_SIZE = 16

export default abstract class Weapon {
  private game: Game

  private name: string

  private infiniteAmmo: boolean

  private ammo: number

  // Temps entre deux tirs (en seconde)
  private baseLatency: number
  private latency: number

  private miniatureIndex: number | null

  constructor(game: Game, name: string) {
    this.game = game

    this.name = name

    this.infiniteAmmo = true
    this.ammo = 0

    this.baseLatency = 0
    this.latency = 0

    this.miniatureIndex = null
  }

  public update(dt: number): void {
    this.updateLatency(dt)
  }

  public getLatencyPercentage(): number {
    return this.latency / this.baseLatency
  }

  public canFire(): boolean {
    return this.hasAmmo() && this.latency <= 0
  }

  public renderMiniature(ctx: CanvasRenderingContext2D): void {
    if (this.miniatureIndex === null)
      return

    const img = this.getGame().getImageManager().getImage('weapons-miniatures')
    const index = this.miniatureIndex

    drawImageByIndex(ctx, img, 0, 0, index, MINIATURE_SIZE, MINIATURE_SIZE)
  }

  public abstract getType(): WeaponName

  public abstract fire(position: Point, angle: number): boolean

  protected hasAmmo(): boolean {
    return this.ammo > 0 || this.hasInfiniteAmmo()
  }

  protected decreaseAmmo(): this {
    if (this.hasInfiniteAmmo())
      return this

    this.ammo--

    if (this.ammo < 0)
      this.ammo = 0

    return this
  }

  protected addAmmo(quantity: number): this {
    this.ammo += quantity

    return this
  }

  protected resetLatency(): this {
    this.latency = this.baseLatency

    return this
  }

  protected setBaseLatency(baseLatency: number): this {
    this.baseLatency = baseLatency

    return this
  }

  protected getGame(): Game {
    return this.game
  }

  protected setMiniatureIndex(miniatureIndex: number): this {
    this.miniatureIndex = miniatureIndex

    return this
  }

  private hasInfiniteAmmo(): boolean {
    return this.infiniteAmmo
  }

  private updateLatency(dt: number): void {
    if (this.latency > 0)
      this.latency -= dt

    if (this.latency < 0)
      this.latency = 0
  }
}
