import Game from '../Game'
import Point from '../Point'
import Bullet from './projectile/Bullet'
import Weapon from './Weapon'
import WeaponName from './WeaponName'

export default class Gun extends Weapon {
  constructor(game: Game) {
    super(game, 'Pistolet')

    this.setBaseLatency(0.5)
    this.setMiniatureIndex(2)
  }

  public getType(): WeaponName {
    return 'gun'
  }

  public fire(position: Point, angle: number): boolean {
    if (!this.canFire())
      return false

    this.decreaseAmmo()
    this.resetLatency()

    const bullet = new Bullet(this.getGame())
    bullet.fire(position, angle)

    return true
  }
}
