import DynamiteBlock from '../block/Dynamite'
import Game from '../Game'
import Point from '../Point'
import Weapon from './Weapon'
import WeaponName from './WeaponName'

export default class Dynamite extends Weapon {
  constructor(game: Game) {
    super(game, 'Dynamite')

    this.setBaseLatency(0.5)
    this.setMiniatureIndex(0)
  }

  public getType(): WeaponName {
    return 'dynamite'
  }

  public fire(position: Point, angle: number): boolean {
    if (!this.canFire())
      return false

    this.decreaseAmmo()
    this.resetLatency()

    const dynamite = new DynamiteBlock(this.getGame(), position.getX(), position.getY())
    this.getGame().addBlock(dynamite)

    return true
  }
}
