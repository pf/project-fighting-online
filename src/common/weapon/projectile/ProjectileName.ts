export type ProjectileName = 'bullet' | 'grenade' | 'rocket' | 'rocket-grenade'
