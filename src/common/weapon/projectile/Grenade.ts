import Game from '../../Game'
import Projectile from './Projectile'

const SLIDER = 0.98
const SPEED_LIMIT_BEFORE_EXPLOSION = 2

export default class Grenade extends Projectile {
  public constructor(game: Game) {
    super(game)

    const image = this.getGame().getImageManager().getImage('projectile/grenade')

    this.setSize(8, 9)
        .setSpeed(400)
        .setImage(image)
  }

  public update(dt: number): void {
    super.update(dt)

    this.slowDown()

    if (this.isTooSlow())
      this.explode()
  }

  private slowDown(): void {
    this.getVelocity().scale(SLIDER)
  }

  private isTooSlow(): boolean {
    return Math.abs(this.getVelocity().getX()) < SPEED_LIMIT_BEFORE_EXPLOSION
  }

  private explode(): void {
    // @todo Effectuer l'explosion

    this.remove()
  }
}
