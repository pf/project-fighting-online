import Explosion from '../../Explosion'
import Game from '../../Game'
import Particle from '../../Particle'
import Projectile from './Projectile'

const SLIDER = 0.98
const INITIAL_SPEED = 400
const SPEED_LIMIT_BEFORE_SMOKE = 50
const SPEED_LIMIT_BEFORE_EXPLOSION = 2

export default class SmokeGrenade extends Projectile {
  public constructor(game: Game) {
    super(game)

    const image = this.getGame().getImageManager().getImage('projectile/smoke-grenade')

    this.setSize(11, 11)
        .setSpeed(INITIAL_SPEED)
        .setImage(image)
  }

  public update(dt: number): void {
    super.update(dt)

    this.slowDown()

    if (this.canSmoke())
      this.smoke()

    if (this.isTooSlow())
      this.remove()
  }

  private slowDown(): void {
    this.getVelocity().scale(SLIDER)
  }

  private canSmoke(): boolean {
    return Math.abs(this.getVelocity().getX()) < SPEED_LIMIT_BEFORE_SMOKE
  }

  private isTooSlow(): boolean {
    return Math.abs(this.getVelocity().getX()) < SPEED_LIMIT_BEFORE_EXPLOSION
  }

  private smoke(): void  {
    const particle = new Particle(this.getGame())
      .setPosition(this.getPosition().clone())
      .setSpeed(24, 68)
      .setLifetime(1, 6)
      .setRadius(20, 60)
      .setAngle(0, 360)

    this.getGame().addParticle(particle)
  }
}
