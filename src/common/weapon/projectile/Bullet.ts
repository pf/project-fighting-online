import Game from '../../Game'
import Projectile from './Projectile'

export default class Bullet extends Projectile {
  private damages: number

  public constructor(game: Game) {
    super(game)

    const image = this.getGame().getImageManager().getImage('projectile/bullet')

    this.setSize(5, 3)
        .setLifetime(3)
        .setSpeed(400)
        .setImage(image)

    this.damages = 10
  }

  public getDamages(): number {
    return this.damages
  }
}
