import Circle from '../../Circle'
import Entity from '../../Entity'
import Game from '../../Game'
import Point from '../../Point'

export default abstract class Projectile implements Entity {
  // Jeu
  private game: Game

  // Position
  private position: Point

  // Déplacement
  private velocity: Point
  private speed: number

  // Durée de vie du projectile
  private lifetime: number | null

  // Angle
  private angle: number

  // Taille
  private width: number
  private height: number

  // Image
  private image: HTMLImageElement | null

  public constructor(game: Game) {
    this.game = game

    this.position = new Point(0, 0)

    this.velocity = new Point(0, 0)
    this.speed = 0

    this.lifetime = null

    this.angle = 0

    this.width = 0
    this.height = 0

    this.image = null
  }

  public update(dt: number): void {
    const velocity = this.getVelocity().clone()
    velocity.scale(dt)

    this.position.add(velocity)

    this.decreaseLifetime(dt)

    if (typeof this.lifetime === 'number' && this.lifetime <= 0)
      this.remove()
  }

  public render(ctx: CanvasRenderingContext2D): void {
    const middle = this.getMiddle()

    ctx.save()

    ctx.translate(this.position.getX(), this.position.getY())
    ctx.translate(middle.getX(), middle.getY())

    if (this.angle !== 0)
      ctx.rotate(this.angle)

    if (this.image) {
      ctx.drawImage(this.image, -middle.getX(), -middle.getY())
    } else {
      ctx.fillStyle = 'rgb(0, 0, 0)'
      ctx.fillRect(0, 0, this.width, this.height)
    }

    ctx.restore()
  }

  public fire(position: Point, angle: number): this {
    this.position = position
    this.angle = angle

    const velocity = new Point(
      Math.cos(this.angle),
      Math.sin(this.angle),
    )

    velocity.scale(this.speed)

    this.velocity = velocity

    this.getGame().addProjectile(this)

    // @todo Lancer le projectile un peu plus loin du joueur, solution
    // temporaire (ou pas)
    this.update(0.08)

    return this
  }

  public remove(): void {
    this.getGame().removeProjectile(this)
  }

  public getVelocity(): Point {
    return this.velocity
  }

  public getCenter(): Point {
    const middle = this.getMiddle()

    return new Point(
      this.position.getX() + middle.getX(),
      this.position.getY() + middle.getY(),
    )
  }

  public getCircle(): Circle {
    const position = this.getCenter()
    const radius = (this.width + this.height) / 4

    return new Circle(position.getX(), position.getY(), radius)
  }

  public getCollisionShape() {
    return this.getCircle()
  }

  protected getGame(): Game {
    return this.game
  }

  protected setImage(image: HTMLImageElement): this {
    this.image = image

    return this
  }

  protected setSize(width: number, height: number): this {
    this.width = width
    this.height = height

    return this
  }

  protected setLifetime(lifetime: number): this {
    this.lifetime = lifetime

    return this
  }

  protected setVelocity(velocity: Point): this {
    this.velocity = velocity

    return this
  }

  protected setSpeed(speed: number): this {
    this.speed = speed

    return this
  }

  protected getPosition(): Point {
    return this.position
  }

  private getMiddle(): Point {
    return new Point(
      this.width / 2,
      this.height / 2,
    )
  }

  private decreaseLifetime(dt: number): void {
    if (typeof this.lifetime !== 'number')
      return

    this.lifetime -= dt

    if (this.lifetime < 0)
      this.lifetime = 0
  }
}
