import Game from '../../Game'
import Particle from '../../Particle'
import Projectile from './Projectile'

export default class Rocket extends Projectile {
  public constructor(game: Game) {
    super(game)

    const image = this.getGame().getImageManager().getImage('projectile/rocket')

    this.setSize(5, 3)
        .setLifetime(30)
        .setSpeed(460)
        .setImage(image)
  }

  public update(dt: number) {
    super.update(dt)

    const particle = new Particle(this.getGame())
      .setPosition(this.getCenter().clone())
      .setSpeed(20, 40)
      .setLifetime(0.4, 0.6)
      .setRadius(1, 3)
      .setAngle(0, 360)
      .setColor([
        'rgb(255, 140, 0)',
        'rgb(200, 100, 0)',
        'rgb(160, 60, 0)',
      ])

    this.getGame().addParticle(particle)
  }

  public explode(): void  {
    // @todo Effectuer l'explosion

    this.remove()
  }
}
