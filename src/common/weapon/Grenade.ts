import Game from '../Game'
import Point from '../Point'
import GrenadeBullet from './projectile/Grenade'
import Weapon from './Weapon'
import WeaponName from './WeaponName'

export default class Grenade extends Weapon {
  constructor(game: Game) {
    super(game, 'Grenade')

    this.setBaseLatency(0.5)
    this.setMiniatureIndex(1)
  }

  public getType(): WeaponName {
    return 'grenade'
  }

  public fire(position: Point, angle: number): boolean {
    if (!this.canFire())
      return false

    this.decreaseAmmo()
    this.resetLatency()

    const grenade = new GrenadeBullet(this.getGame())
    grenade.fire(position, angle)

    return true
  }
}
