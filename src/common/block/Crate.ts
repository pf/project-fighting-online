import Game from '../Game'
import Block from './Block'

export default class Crate extends Block {
  public constructor(game: Game, x: number, y: number) {
    super(game, x, y, 25, 25)

    this
      .setSolid(true)
      .setImage(game.getImageManager().getImage('block/crate'))
  }
}
