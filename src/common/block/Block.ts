import Circle from '../Circle'
import Entity from '../Entity'
import Game from '../Game'
import Point from '../Point'

export default abstract class Block implements Entity {
  private game: Game

  private position: Point

  private width: number
  private height: number

  private image: HTMLImageElement | null

  private solid: boolean

  public constructor(game: Game, x: number, y: number, width: number, height: number) {
    this.game = game

    this.position = new Point(x, y)

    this.width = width
    this.height = height

    this.image = null

    this.solid = true
  }

  public update(dt: number): void { }

  public render(ctx: CanvasRenderingContext2D): void {
    if (!this.image)
      return

    const middle = this.getMiddle()

    ctx.save()

    ctx.translate(this.position.getX(), this.position.getY())
    ctx.translate(middle.getX(), middle.getY())

    ctx.drawImage(this.image, -middle.getX(), -middle.getY())

    ctx.restore()
  }

  public isSolid(): boolean {
    return this.solid
  }

  public getPosition(): Point {
    return this.position
  }

  public getCenter(): Point {
    const middle = this.getMiddle()

    return new Point(
      this.position.getX() + middle.getX(),
      this.position.getY() + middle.getY(),
    )
  }

  public getCircle(): Circle {
    const position = this.getCenter()
    const radius = (this.width + this.height) / 4

    return new Circle(position.getX(), position.getY(), radius)
  }

  public getCollisionShape() {
    return this.getCircle()
  }

  protected getGame(): Game {
    return this.game
  }

  protected setSize(width: number, height: number): this {
    this.width = width
    this.height = height

    return this
  }

  protected setImage(image: HTMLImageElement): this {
    this.image = image

    return this
  }

  protected setSolid(solid: boolean): this {
    this.solid = solid

    return this
  }

  private getMiddle(): Point {
    return new Point(
      this.width / 2,
      this.height / 2,
    )
  }
}
