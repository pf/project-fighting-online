import Game from '../Game'
import Mine from './Mine'

export default class Dynamite extends Mine {
  constructor(game: Game, x: number, y: number) {
    super(game, x, y)

    this
      .setSize(5, 12)
      .setImage(game.getImageManager().getImage('block/dynamite'))
      .setTimeBeforeCanExplode(0)
      .setTimeBeforeExplosion(4)
      .setActive(true)
  }
}
