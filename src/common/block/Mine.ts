import Explosion from '../Explosion'
import Game from '../Game'
import Block from './Block'

export default class Mine extends Block {
  // Temps durant lequel, après la pose, la mine est totalement inactive
  //
  // Permet de faire en sorte que le joueur, qui est sur la mine au moment de
  // la pose, ne la fasse pas immédiatement exploser
  private timeBeforeCanExplode: number

  // Temps de latence entre l'activation du bloc et son explosion
  private timeBeforeExplosion: number

  // Le bloc est-il activé ? (activé = prêt à exploser)
  private active: boolean

  public constructor(game: Game, x: number, y: number) {
    super(game, x, y, 10, 10)

    this.timeBeforeCanExplode = 1
    this.timeBeforeExplosion = 0.6
    this.active = false

    this
      .setSolid(false)
      .setImage(game.getImageManager().getImage('block/mine'))
  }

  public update(dt: number): void {
    super.update(dt)

    if (this.canExplode()) {
      if (this.active) {
        this.timeBeforeExplosion -= dt

        if (this.timeBeforeExplosion <= 0)
          this.explode()
      }
    } else {
      this.timeBeforeCanExplode -= dt

      if (this.timeBeforeCanExplode < 0)
        this.timeBeforeCanExplode = 0
    }
  }

  public explode(): void {
    const explosion = new Explosion(this.getGame(), this.getPosition().clone())
    explosion.explode()

    this.getGame().removeBlock(this)
  }

  public setActive(active: boolean): this {
    this.active = active

    return this
  }

  public canExplode(): boolean {
    return this.timeBeforeCanExplode === 0
  }

  protected setTimeBeforeExplosion(timeBeforeExplosion: number): this {
    this.timeBeforeExplosion = timeBeforeExplosion

    return this
  }

  protected setTimeBeforeCanExplode(timeBeforeCanExplode: number): this {
    this.timeBeforeCanExplode = timeBeforeCanExplode

    return this
  }
}
