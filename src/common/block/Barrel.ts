import Explosion from '../Explosion'
import Game from '../Game'
import Block from './Block'

const BASE_HEALTH = 240

export default class Barrel extends Block {
  private health: number

  public constructor(game: Game, x: number, y: number) {
    super(game, x, y, 25, 25)

    this
      .setSolid(true)
      .setImage(game.getImageManager().getImage('block/barrel'))

    this.health = BASE_HEALTH
  }

  public update(dt: number): void {
    super.update(dt)

    if (this.health <= 0)
      this.explode()
  }

  public decreaseHealth(value: number): void {
    this.health -= value

    if (this.health < 0)
      this.health = 0
  }

  public explode(): void {
    const explosion = new Explosion(this.getGame(), this.getPosition().clone())
    explosion.explode()

    this.getGame().removeBlock(this)
  }
}
