import Game from './Game'
import Particle from './Particle'
import Point from './Point'

export default class Explosion {
  private game: Game
  private position: Point

  public constructor(game: Game, position: Point) {
    this.game = game
    this.position = position
  }

  // Déclenche l'explosion
  public explode(): void {
    const particles: Array<Particle> = []

    /*

    setSeed(rand(0, 100000))

    console.log(rand(20, 50))
    console.log(rand(20, 50))
    console.log(rand(10, 60))

    */

    for (let i = 0 ; i < 40 ; i++) {
      const particle = new Particle(this.game)
        .setPosition(this.position.clone())
        .setSpeed(12, 32)
        .setLifetime(0.6, 2)
        .setRadius(12, 24)
        .setAngle(0, 360)
        .setDamages(3)

      particles.push(particle)

      // @todo Faire une autre boucle après pour faire ça
      this.game.addParticle(particle)
    }
  }
}
